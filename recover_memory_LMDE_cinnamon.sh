#!/usr/bin/env bash

### Recover cached memory on a running Debian (e.g., LMDE) box with a Cinnamon desktop.
### TODO:
### * report memory use before and after run
### * test helper retvals/errorcodes
### * add helpers to safely-close specific memory-hungry apps (e.g., Firefox)
### * add logging

### constants

## messaging

THIS_FP="${0}"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## define helpers here

DESKTOP_FREE="${THIS_DIR}/recover_memory_cinnamon.sh"
CACHE_FREE="${THIS_DIR}/recover_memory_debian_cache.sh"
SWAP_FREE="${THIS_DIR}/recover_memory_swap.sh"

### payload

THIS_START="$(date)"
echo -e "${MESSAGE_PREFIX} START=${THIS_START}\n"

## run helpers here
for CMD in \
    "sudo ${DESKTOP_FREE}" \
    "sudo ${CACHE_FREE}" \
    "sudo ${SWAP_FREE}" \
; do
    echo -e "${MESSAGE_PREFIX} $(date): about to run '${CMD}'"
    ${CMD} # don't quote
    # TODO: test retvals/errorcodes
    echo -e "${MESSAGE_PREFIX} $(date): finished run '${CMD}'"
    echo # newline
done
    
echo -e "${MESSAGE_PREFIX} START=${THIS_START}"
echo -e "${MESSAGE_PREFIX}   END=$(date)"
exit 0
