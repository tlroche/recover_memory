#!/usr/bin/env bash

### Make a Debian system drop OS cache contents (increasing free memory).
### See, e.g.,
### - https://unix.stackexchange.com/q/45441/38638 (archived @ https://web.archive.org/web/20220222160649/https://unix.stackexchange.com/questions/45441/what-command-can-be-used-to-force-release-everything-in-swap-partition-back-to-m )
### - https://www.linux.org/docs/man8/swapon.html (not archived, just a live webpage backup of `swapoff`+`swapon` manpage)

### TODO:
### - update to newer `swapoff`+`swapon` supporting `--discard`
### - test retvals/errorcodes inside `if`, but probably without looping (gotta prevent subshells?)

### constants

## messaging

THIS_FP="${0}"
# THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### payload

if [[ $(id -u) -eq 0 ]] ; then
    ## We are now root, so can do ...
    echo "Using from $(swapoff --version)"       # update!
    echo " Pre-swap-cycle @ $(date): swapspace=" # lead space for alignment
    swapon --summary
    echo # newline
    echo 'About to swapcycle=[`swapoff --all`, `swapon --all`] ...'
    swapoff --all
    swapon --all
    echo # newline
    echo "Post-swap-cycle @ $(date): swapspace=" # lead space for alignment
    swapon --summary
    exit 0
else
    echo -e "${ERROR_PREFIX}: must be run as root, exiting..." 1>&2
    exit 1
fi
