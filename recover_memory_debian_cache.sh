#!/usr/bin/env bash

### Make a Debian system drop OS cache contents (increasing free memory).

### constants

## messaging

THIS_FP="${0}"
# THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### payload

if [[ $(id -u) -eq 0 ]] ; then
    # we are now root, so can do
    echo 3 > /proc/sys/vm/drop_caches
    # TODO: test retval/errorcode
    exit 0
else
    echo -e "${ERROR_PREFIX}: must be run as root, exiting..." 1>&2
    exit 1
fi
