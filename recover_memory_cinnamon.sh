#!/usr/bin/env bash

###
### TODO: do not run until bug with cinnamon version=2.8.6 is fixed, replaced, or workarounded!
###       See (e.g.) http://forums.linuxmint.com/viewtopic.php?f=238&t=216001
###

### Recover cached memory on a running Debian (e.g., LMDE) box with a Cinnamon desktop.

### constants

## messaging

THIS_FP="${0}"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### payload

echo "${MESSAGE_PREFIX} not running due to 'cinnamon --replace' problems"

# THIS_START="$(date)"
# echo "${MESSAGE_PREFIX} START=${THIS_START}"

## restart Cinnamon
# echo "${MESSAGE_PREFIX} DO NOT use keyboard or mouse while Cinnamon is unloading!"
## Before `cinnamon --version`==2.8.6, following worked: particularly, it returned.
# cinnamon --replace
## As of `cinnamon --version`==2.8.6, above hangs, and C-c kills Cinnamon.
# cinnamon --replace &
## As of `cinnamon --version`==2.8.6, above cripples desktop
# killall -9 cinnamon
# TODO: test retval/errorcode
# echo "${MESSAGE_PREFIX} Cinnamon unloaded: resume normal IO"

# echo "${MESSAGE_PREFIX} START=${THIS_START}"
# echo "${MESSAGE_PREFIX}   END=$(date)"
exit 0
