.. contents:: **Table of Contents**

summary
=======

A collection of small bash_ scripts for freeing `Linux`_ memory when needed. Currently oriented toward use with `LMDE`_ (a `Debian`_ `distro`_) and `Cinnamon`_ (a `GNOME`_ `desktop`_), which I use.

.. _bash: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
.. _Cinnamon: https://en.wikipedia.org/wiki/Cinnamon_%28software%29
.. _Debian: https://en.wikipedia.org/wiki/Debian
.. _desktop: https://en.wikipedia.org/wiki/Desktop_environment#Desktop_environments_for_the_X_Window_System
.. _distro: https://en.wikipedia.org/wiki/Linux_distribution
.. _GNOME: https://en.wikipedia.org/wiki/GNOME
.. _Linux: https://en.wikipedia.org/wiki/Linux
.. _LMDE: https://en.wikipedia.org/wiki/Linux_Mint#Linux_Mint_Debian_Edition

motivation
==========

Periodically free memory on my main workstation (a laptop running `LMDE`_ with `Cinnamon`_) will get low, mostly due to my usage of `Firefox`_ tabs being wildly out of balance with the box's amount of RAM. (Note this seems to be mostly about undisciplined tab usage, not Firefox: when I run `Chrome`_, I tend to do the same thing, and free memory decreases similarly, though a bit more slowly.) When this happens, I quit Firefox (saving open tabs but not closed tabs), run one or more of the scripts in this repo (typically `recover_memory_LMDE_cinnamon.sh`_), then restart Firefox. Other apps (notably `Emacs`_) seem unaffected, and performance improves ... until my various uses once again sufficiently decrease free memory.

.. _Chrome: https://en.wikipedia.org/wiki/Google_Chrome
.. _Emacs: https://en.wikipedia.org/wiki/Emacs
.. _Firefox: https://en.wikipedia.org/wiki/Firefox
.. relative linking to source file
.. _recover_memory_LMDE_cinnamon.sh: ./recover_memory_LMDE_cinnamon.sh

scripts
=======

They're quite short and (IMHO) readable, so just read the `sources`_.

.. relative linking to folder=Source
.. _sources: ../../src

your assistance appreciated
===========================

If you've got

* improvements for any current scripts
* scripts for other platforms (e.g., desktops, distros)

please fork and make a pull request!
